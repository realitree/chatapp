package de.djindal.chatapp.services;

import com.vaadin.flow.server.auth.AnonymousAllowed;
import de.djindal.chatapp.model.Chat;
import de.djindal.chatapp.model.Message;
import dev.hilla.BrowserCallable;
import dev.hilla.Nonnull;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;

@BrowserCallable
@AnonymousAllowed
@Service
public class ChatController {
    private final ChatService chatService;

    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }


    public void sendMessage(String topic, String author, String message) {
        chatService.sendMessage(topic, author, message);
    }

    public Chat createChat(String topic) {
        return chatService.createChat(topic);
    }

    public @Nonnull List<@Nonnull String> getTopics() {
        return chatService.getTopics();
    }

    public @Nonnull Flux<@Nonnull Message> joinChat(String topic) {
        return chatService.joinChat(topic);
    }

    public List<Message> getChatHistory(String topic) {
        return chatService.getChatHistory(topic);
    }

}
