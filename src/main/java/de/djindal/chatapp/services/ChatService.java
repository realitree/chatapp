package de.djindal.chatapp.services;

import com.helger.commons.annotation.Singleton;
import de.djindal.chatapp.model.Chat;
import de.djindal.chatapp.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
@Service
public class ChatService {
    private final Map<String, Sinks.Many<Message>> activeChatSinks = new HashMap<>();
    private final ChatRepository chatRepository;

    public ChatService(@Autowired ChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }


    public Chat createChat(String topic) {
        Chat chat = new Chat(topic);
        return chatRepository.save(chat);
    }

    private void createChatSink(String topic) {
        Sinks.Many<Message> chatSink = Sinks.many().multicast().directBestEffort();
        activeChatSinks.put(topic, chatSink);
    }

    public void sendMessage(String topic, String author, String message) {
        var chat = chatRepository.findByTopic(topic);
        chat.getMessages().add(new Message(author, message));
        if (activeChatSinks.containsKey(topic)) {
            activeChatSinks.get(topic).tryEmitNext(new Message(author, message));
        }
        chatRepository.save(chat);
    }

    public List<Message> getChatHistory(String topic) {
        var chat = chatRepository.findByTopic(topic);
        return chat.getMessages();
    }

    public Flux<Message> joinChat(String topic) {
        if (!activeChatSinks.containsKey(topic)) {
            createChatSink(topic);
        }
        return activeChatSinks.get(topic).asFlux();
    }

    public List<String> getTopics() {
        return chatRepository.findAll().stream().map(Chat::getTopic).toList();
    }

}
