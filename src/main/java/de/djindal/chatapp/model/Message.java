package de.djindal.chatapp.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity
@Getter
@NoArgsConstructor
public class Message {
    @Id
    @GeneratedValue
    private UUID id;
    private String text;
    private String author;

    public Message(String author, String text) {
        this.author = author;
        this.text = text;
    }
}
