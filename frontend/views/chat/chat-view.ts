import {css, html} from 'lit';
import {customElement, state} from 'lit/decorators.js';
import { View } from '../../views/view.js';
import {BeforeEnterObserver, RouterLocation } from '@vaadin/router';
import Message from "Frontend/generated/de/djindal/chatapp/model/Message";
import {ChatController} from "Frontend/generated/endpoints";


@customElement('chat-view')
export class ChatView extends View implements BeforeEnterObserver {
  @state()
  private topic : string = '';
  @state()
  private chatHistory : Message[] = [];

  static styles = css`
      
  `;


  onBeforeEnter(location: RouterLocation) {
    this.topic = location.params['topic'] as string;
  }

  connectedCallback() {
    super.connectedCallback();
    this.classList.add(
        'flex',
        'flex-col',
        'h-full',
        'p-l',
    );
    this.getChatHistory();
      ChatController.joinChat(this.topic).onNext((msg) => {
          this.chatHistory = [...this.chatHistory, msg];
      });
  }

  render() {
    return html`
      <h2 style="padding-bottom: 1em;"> ${this.topic} </h2>
      <div class="chat-history-container">
          <div class="message  message-header">
              <div class="author">Sender</div>
              <div class="text">Content</div>
          </div>
        ${this.chatHistory.map((message) =>
            html`
              <div class="message">
                <div class="author">${message.author}</div>
                <div class="text">${message.text}</div>
              </div>
            `
        )}
      </div>
      <div class="new-message-container">
        <vaadin-text-field id="author-input" label="Sender"></vaadin-text-field>
        <vaadin-text-field id="message-input" label="Content"></vaadin-text-field>
        <vaadin-button @click="${this.sendMessage}">Send</vaadin-button>
      </div>
      <a href="../chats">Back to chats</a>
    `;
  }

    async getChatHistory() {
        this.chatHistory = await ChatController.getChatHistory(this.topic);
    }

    async sendMessage() {
        let authorField = document.getElementById('author-input') as HTMLInputElement;
        let messageField = document.getElementById('message-input') as HTMLInputElement;
        const author = authorField.value;
        const message = messageField.value;
        await ChatController.sendMessage(this.topic, author, message);
        messageField.value = '';
        this.getChatHistory();
    }
}
