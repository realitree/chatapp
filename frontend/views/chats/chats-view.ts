import '@vaadin/button';
import '@vaadin/notification';
import '@vaadin/text-field';
import { ChatController } from 'Frontend/generated/endpoints.js';
import { html } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { View } from '../../views/view.js';

@customElement('chats-view')
export class ChatsView extends View {
  @state()
  private topics: String[] = [];

  connectedCallback() {
    super.connectedCallback();
    this.classList.add('flex', 'p-m', 'gap-m', 'flex-col');
    this.getTopics();
  }

  render() {
    return html`
      <h2>Existing Chats</h2>
      <h3>Topics</h3>
      
      <ul>
      ${this.topics.map((topic) =>
          html`<li><a href="${'../chat/' + topic}"> ${topic}</a></li>`
      )}
      </ul>
      <div class="new-chat-container">
        <vaadin-text-field id="topic-input" label="Topic"></vaadin-text-field>
        <vaadin-button @click="${this.createChat}">Create Chat</vaadin-button>
      </div>
      
    `;
  }

  async createChat() {
    let topicField = document.getElementById('topic-input') as HTMLInputElement;
    const createdChat = await ChatController.createChat(topicField.value);
    if (createdChat?.topic == null) {
      return;
    }
    this.topics = [...this.topics, createdChat.topic];
  }

  async getTopics() {
    let topics = await ChatController.getTopics();
    this.topics = topics;
  }

}
