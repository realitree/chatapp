import type { Route } from '@vaadin/router';
import 'Frontend/views/chats/chats-view';
import './views/main-layout';

export type ViewRoute = Route & {
  title?: string;
  icon?: string;
  children?: ViewRoute[];
};

export const views: ViewRoute[] = [
  // Place routes below (more info https://hilla.dev/docs/routing)
  {
    path: '',
    component: 'chats-view',
    icon: '',
    title: '',
  },
  {
    path: 'chats',
    component: 'chats-view',
    icon: 'globe-solid',
    title: 'Chats',
  },
  {
    path: 'chat/:topic?',
    component: 'chat-view',
    icon: 'file',
    title: 'Chat',
    action: async (_context, _command) => {
      await import('Frontend/views/chat/chat-view');
      return;
    },
  },
];
export const routes: ViewRoute[] = [
  {
    path: '',
    component: 'main-layout',
    children: views,
  },
];
